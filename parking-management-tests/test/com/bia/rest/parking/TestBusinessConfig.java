package com.bia.rest.parking;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

import com.bia.business.api.parking.ParkingLotBusiness;
import com.bia.business.impl.parking.ParkingLotBusinessImpl;


/**
 * @author Andrada Radu
 * Test configuration class
 */
@TestConfiguration
public class TestBusinessConfig {

    @Bean
    public ParkingLotBusiness parkingLotBusiness() {
        return new ParkingLotBusinessImpl();
    }
}