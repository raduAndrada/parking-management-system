package com.bia.rest.parking;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import com.bia.business.api.parking.ParkingLotBusiness;
import com.bia.business.impl.parking.exceptions.InvalidParkingSpotArgumentException;
import com.bia.model.base.ImtSpotUpdateDetails;
import com.bia.model.parking.Floor;
import com.bia.model.parking.ImtFloor;
import com.bia.model.parking.ImtParkingLot;
import com.bia.model.parking.ImtRow;
import com.bia.model.parking.ImtSpot;
import com.bia.model.parking.ParkingLot;
import com.bia.model.parking.Row;
import com.bia.model.parking.Spot;
import com.bia.model.parking.SpotSize;
import com.bia.model.vehicle.ImtVehicle;
import com.bia.model.vehicle.VehicleType;
import com.google.common.collect.ImmutableList;

@RunWith(SpringRunner.class)
@SpringBootTest
@Import(TestBusinessConfig.class)
public class ParkingLotBusinessTests {

	@Autowired
	private ParkingLotBusiness parkingLotBusiness;

	private ParkingLot parkingLot0;
	private ParkingLot parkingLot1;
	private ParkingLot parkingLot2;

	@Before
	public void init() {

		final Spot spot0 = buildSpot(1, 1, 1, SpotSize.BIG, true);
		final Spot spot1 = buildSpot(2, 1, 1, SpotSize.BIG, false);
		final Spot spot2 = buildSpot(3, 1, 1, SpotSize.SMALL, true);

		final Spot spot3 = buildSpot(1, 2, 1, SpotSize.SMALL, false);
		final Spot spot4 = buildSpot(2, 2, 1, SpotSize.SMALL, false);
		final Spot spot5 = buildSpot(3, 2, 1, SpotSize.BIG, true);

		final Spot spot6 = buildSpot(1, 3, 1, SpotSize.BIG, false);
		final Spot spot7 = buildSpot(2, 3, 1, SpotSize.BIG, true);

		final Spot spot8 = buildSpot(3, 3, 1, SpotSize.BIG, true);
		final Spot spot9 = buildSpot(3, 3, 1, SpotSize.BIG, false);

		final Spot spot10 = buildSpot(2, 3, 1, SpotSize.BIG, false);

		final List<Spot> spotList0 = ImmutableList.of(spot0, spot1, spot2);
		final List<Spot> spotList1 = ImmutableList.of(spot3, spot4, spot5);
		final List<Spot> spotList2 = ImmutableList.of(spot6, spot7, spot8);

		final List<Spot> spotList3 = ImmutableList.of(spot6, spot7, spot9);
		final List<Spot> spotList4 = ImmutableList.of(spot6, spot10, spot9);

		final Row row0 = buildRow(1, spotList0);
		final Row row1 = buildRow(2, spotList1);
		final Row row2 = buildRow(3, spotList2);
		final Row row3 = buildRow(3, spotList3);
		final Row row4 = buildRow(3, spotList4);

		final Floor floor0 = buildFloor(1, ImmutableList.of(row0, row1, row2));
		final Floor floor1 = buildFloor(1, ImmutableList.of(row0, row1, row3));
		final Floor floor2 = buildFloor(1, ImmutableList.of(row0, row1, row4));

		parkingLot0 = ImtParkingLot.builder().addFloorList(floor0).build();
		parkingLot1 = ImtParkingLot.builder().addFloorList(floor1).build();
		parkingLot2 = ImtParkingLot.builder().addFloorList(floor2).build();

	}

	@Test
	public void find_spot_for_motorbike_ok() {
		final Spot expectedSpot = buildSpot(1, 1, 1, SpotSize.BIG, true);
		final Spot actualSpot = this.parkingLotBusiness
				.getAvailableSpots(parkingLot0, ImtVehicle.builder().type(VehicleType.MOTORBIKE).build()).stream()
				.findFirst().get();
		assertThat(expectedSpot).isEqualTo(actualSpot);
	}

	@Test
	public void find_spot_for_car_ok() {
		final Spot expectedSpot = buildSpot(1, 1, 1, SpotSize.BIG, true);
		final Spot actualSpot = this.parkingLotBusiness
				.getAvailableSpots(parkingLot0, ImtVehicle.builder().type(VehicleType.CAR).build()).stream().findFirst()
				.get();
		assertThat(expectedSpot).isEqualTo(actualSpot);
	}

	@Test
	public void find_spot_for_bus_ok() {
		final Spot expectedSpot = buildSpot(2, 3, 1, SpotSize.BIG, true);
		final Spot actualSpot = this.parkingLotBusiness
				.getAvailableSpots(parkingLot0, ImtVehicle.builder().type(VehicleType.BUS).build()).stream().findFirst()
				.get();
		assertThat(expectedSpot).isEqualTo(actualSpot);
	}

	@Test
	public void find_spot_for_bus_not_ok() {
		final List<Spot> actualSpots = this.parkingLotBusiness.getAvailableSpots(parkingLot1,
				ImtVehicle.builder().type(VehicleType.BUS).build());
		assertThat(actualSpots).isEqualTo(ImmutableList.of());
	}

	@Test
	public void update_parking_lot_ok() {
		final ParkingLot actualResult = parkingLotBusiness.updateParkingLotSpots(parkingLot0,
				ImtSpotUpdateDetails.builder().floorNumber(1).rowNumber(3).spotNumber(3).isBusSpot(false).build());
		assertThat(actualResult).isEqualTo(parkingLot1);
	}

	@Test
	public void update_parking_lot_bus_ok() {
		final ParkingLot actualResult = parkingLotBusiness.updateParkingLotSpots(parkingLot0,
				ImtSpotUpdateDetails.builder().floorNumber(1).rowNumber(3).spotNumber(2).isBusSpot(true).build());
		assertThat(actualResult).isEqualTo(parkingLot2);
	}

	@Test(expected = InvalidParkingSpotArgumentException.class)
	public void update_parking_not_ok() {
		parkingLotBusiness.updateParkingLotSpots(parkingLot0,
				ImtSpotUpdateDetails.builder().floorNumber(1).rowNumber(4).spotNumber(3).isBusSpot(false).build());
	}

	private Spot buildSpot(int spotNumber, int rowNumber, int floorNumber, SpotSize size, boolean free) {
		return ImtSpot.builder().number(spotNumber).rowNumber(rowNumber).floorNumber(floorNumber).isFree(free)
				.size(size).build();
	}

	private Row buildRow(int rowNumber, List<Spot> spotList) {
		return ImtRow.builder().spotList(spotList).number(rowNumber).build();
	}

	private Floor buildFloor(int floorNumber, List<Row> rowList) {
		return ImtFloor.builder().rowList(rowList).number(floorNumber).build();
	}

}
