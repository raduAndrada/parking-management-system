# parking-management-system


This is a small application containing a server and a client. The server is structured using a layered architecture and the client is an angular application.
There are 3 types of vehicles: small, medium and large. Each of them requires a parking lot depending on the size. This application generates a random parking area and sends any vehicle to the closest free spot. 
The functionality of this can be observed on the client application. 
Requirements: 
- Gradle
- Java

Installation:
- run gradle build
- start the server
- the frontend application is deployed on firebase and can be found at https://parking-management-syste-105f5.firebaseapp.com/