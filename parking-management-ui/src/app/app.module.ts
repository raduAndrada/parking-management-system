import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing.module';

import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ParkingComponent } from './parking/parking-lot/parking.component';
import { ParkingService } from './parking/parking.service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { SpotComponent } from './parking/parking-lot/spot/spot.component';
import { RowComponent } from './parking/parking-lot/row/row.component';
import { FloorComponent } from './parking/parking-lot/floor/floor.component';

@NgModule({
  declarations: [
    AppComponent,
    ParkingComponent,
    SpotComponent,
    RowComponent,
    FloorComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
  ],
  providers: [ ParkingService],
  bootstrap: [AppComponent]
})
export class AppModule { }
