import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { ParkingComponent } from './parking/parking-lot/parking.component';

const appRoutes: Routes = [
    { path: 'parking-lot', component: ParkingComponent, pathMatch: "full"},
    { path: "" , redirectTo: "/home", pathMatch: "full"},

];
@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes/*, {useHash: true}*/)
    ],
    exports: [ RouterModule]
})
export class AppRoutingModule {

}


