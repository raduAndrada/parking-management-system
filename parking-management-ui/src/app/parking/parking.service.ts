import {  HttpClient } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Subject, Observable, of } from 'rxjs';
import { ParkingLot, Spot, ParkingLotUpdateDetails } from './parking.model';
import { environment } from '../../environments/environment';

@Injectable()
export class ParkingService {

    private parkingLotUrl = environment.serverPort + '/v1/parking-lot';



    constructor( private http: HttpClient) {
    }

      getParkingLot(): Observable<ParkingLot> {
          return this.http.get<ParkingLot>( this.parkingLotUrl)
          .pipe(
                  tap(_ => console.log(_)),
                  catchError(this.handleError<ParkingLot>('getParkingLot'))
                );
      }

      getSpot(vehicleType: string): Observable<Spot> {

        const url = this.parkingLotUrl + '/available-spot' + '?vehicleType=' + vehicleType;
        return this.http.get<Spot>(url)
        .pipe(
                tap(_ => console.log(_)),
                catchError(this.handleError<Spot>('getSpot'))
              );
       }


      updateParkingLot(updateDetails: ParkingLotUpdateDetails): Observable<ParkingLotUpdateDetails> {
      const url = this.parkingLotUrl;
      return this.http.put<ParkingLotUpdateDetails>(url, updateDetails)
      .pipe(
              tap(_ => console.log(_)),
              catchError(this.handleError<ParkingLotUpdateDetails>('updateParkingLot'))
            );
        }

      private handleError<T> (operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
          console.error(error); // log to console instead
          return of(result as T);
        };
      }

}
