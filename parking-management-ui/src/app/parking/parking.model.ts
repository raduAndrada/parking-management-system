export interface ParkingLot {

    floorList: Floor[];
}

export interface Floor {

    number: number;

    rowList: Row[];
}

export interface Row {

    number: number;

    spotList: Spot[];
}

export interface Spot {

	floorNumber : number;

	rowNumber: number;

    number: number;

    size: string;
    
    isFree : boolean;
}

export interface ParkingLotUpdateDetails {
    
    floorNumber : number;

	rowNumber: number;

    spotNumber: number;

    isBusSpot: boolean;
}
