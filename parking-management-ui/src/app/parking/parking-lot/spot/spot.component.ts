import { Component, OnInit, Input } from '@angular/core';
import { Spot } from '../../parking.model';

@Component({
  selector: 'app-spot',
  templateUrl: './spot.component.html',
  styleUrls: ['./spot.component.css']
})
export class SpotComponent implements OnInit {

  @Input() spot: Spot;
  
  constructor() { }

  ngOnInit() {
  }

}
