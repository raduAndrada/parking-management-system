import { Component, OnInit, Input } from '@angular/core';
import { Floor } from '../../parking.model';

@Component({
  selector: 'app-floor',
  templateUrl: './floor.component.html',
  styleUrls: ['./floor.component.css']
})
export class FloorComponent implements OnInit {

  @Input() floor: Floor;

  constructor() { }

  ngOnInit() {
  }

}
