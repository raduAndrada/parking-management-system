import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ParkingService } from '../parking.service';
import { ParkingLot, Spot } from '../parking.model';

@Component({
  selector: 'app-parking',
  templateUrl: './parking.component.html',
  styleUrls: ['./parking.component.css']
})
export class ParkingComponent implements OnInit {
  
  parkingLot = null ;
  spot = null;
  model = "";

  searchClicked = false;


  constructor( 
    private route: ActivatedRoute,
    private parkingService: ParkingService) {
      this.parkingService.getParkingLot()
      .subscribe(
          (parkingLot: ParkingLot ) => {this.parkingLot = parkingLot ; });
     }


  ngOnInit() {

  }


  findSpot() {
    this.searchClicked = true;
    this.parkingService.getSpot(this.model)
      .subscribe(
          (spot: Spot ) => {this.spot = spot ; });
  }



}
