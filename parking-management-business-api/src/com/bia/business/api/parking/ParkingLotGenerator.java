package com.bia.business.api.parking;

import com.bia.model.parking.ParkingLot;

/**
 * @author Andrada Radu 
 * Generator for the initial state of the parking lot
 */
public interface ParkingLotGenerator {

	/**
	 * @return an initial parking lot
	 */
	public ParkingLot generateParkingLot();

}
