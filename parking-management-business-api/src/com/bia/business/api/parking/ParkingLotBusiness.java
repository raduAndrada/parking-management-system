package com.bia.business.api.parking;

import java.util.List;

import com.bia.model.base.SpotUpdateDetails;
import com.bia.model.parking.ParkingLot;
import com.bia.model.parking.Spot;
import com.bia.model.vehicle.Vehicle;

/**
 * @author Andrada Radu
 */
public interface ParkingLotBusiness {

	/**
	 * Get the list of available free spots
	 * 
	 * @return a list with all the available spots in the parking lot
	 */
	public List<Spot> getAvailableSpots(final ParkingLot parkingLot, final Vehicle vehicle);

	/**
	 * Update state of the parking lot
	 * 
	 * @param parkingLot    the parking lot to be updated
	 * @param updateDetails the details of the spot being updated
	 * @return the updated parking lot
	 */
	public ParkingLot updateParkingLotSpots(final ParkingLot parkingLot, final SpotUpdateDetails updateDetails);

}
