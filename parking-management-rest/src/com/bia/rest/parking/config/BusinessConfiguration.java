package com.bia.rest.parking.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.bia.business.api.parking.ParkingLotBusiness;
import com.bia.business.api.parking.ParkingLotGenerator;
import com.bia.business.impl.parking.ParkingLotBusinessImpl;
import com.bia.business.impl.parking.ParkingLotGeneratorImpl;

/**
 * 
 * @author Andrada Radu
 * Configuration 
 */
@Configuration
public class BusinessConfiguration {

	/**
	 * @return the bean with the business implementation
	 */
	@Bean
	public ParkingLotBusiness parkingLotBusiness() {
		return new ParkingLotBusinessImpl();
	}
	
	/**
	 * @return generator for a parking lot 
	 */
	@Bean
	public ParkingLotGenerator parkingLotGenerator() {
		return new ParkingLotGeneratorImpl();
	}
	
	

}
