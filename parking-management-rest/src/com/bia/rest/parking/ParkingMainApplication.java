package com.bia.rest.parking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 
 * @author Andrada Radu
 * Main application
 */
@SpringBootApplication
public class ParkingMainApplication {


		public static void main(final String[] args) {
			SpringApplication.run(ParkingMainApplication.class, args);
		}
	

}
