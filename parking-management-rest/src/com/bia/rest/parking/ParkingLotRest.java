package com.bia.rest.parking;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bia.business.api.parking.ParkingLotBusiness;
import com.bia.business.api.parking.ParkingLotGenerator;
import com.bia.model.base.SpotUpdateDetails;
import com.bia.model.parking.ParkingLot;
import com.bia.model.parking.Spot;
import com.bia.model.vehicle.ImtVehicle;
import com.bia.model.vehicle.VehicleType;

/**
 * 
 * @author Andrada Radu Controller for the parking lot
 */
@RestController
@RequestMapping(value = "/v1/parking-lot")
@CrossOrigin
public class ParkingLotRest {

	@Autowired
	private ParkingLotBusiness parkingLotBusiness;

	@Autowired
	private ParkingLotGenerator parkingLotGenerator;

	private ParkingLot parkingLot;

	/**
	 * Init a parking lot for the application
	 */
	@PostConstruct
	public void initParkingLot() {
		this.parkingLot = parkingLotGenerator.generateParkingLot();
	}

	/**
	 * Get the initial parking lot
	 * 
	 * @return the generated parking lot
	 */
	@GetMapping
	public ResponseEntity<ParkingLot> getParkingLot() {
		return ResponseEntity.ok(this.parkingLot);
	}

	/**
	 * Get the 1st available spot
	 * 
	 * @param vehicleType can be CAR,BUS or MOTORBIKE
	 * @return the 1st spot that satisfies the requirements
	 */
	@GetMapping("available-spot")
	public ResponseEntity<Spot> getSpot(@RequestParam("vehicleType") final VehicleType vehicleType) {
		final Spot spot = parkingLotBusiness
				.getAvailableSpots(this.parkingLot, ImtVehicle.builder().type(vehicleType).build()).stream().findFirst()
				.orElse(null);
		// the ui handles the display of the message if no spots are found
		// could send a 404 status
		return ResponseEntity.ok(spot);
	}

	/**
	 * Update the parking lot when a vehicle occupies/frees a spot
	 * 
	 * @param updateDets the details of the spot freed/occupied
	 * @return true if the update was successful
	 */
	@PutMapping
	public ResponseEntity<Boolean> updateSpot(@RequestBody final SpotUpdateDetails updateDets) {
		parkingLotBusiness.updateParkingLotSpots(this.parkingLot, updateDets);
		return ResponseEntity.ok(Boolean.TRUE);
	}

}
