package parking;


import java.util.List;

import org.immutables.value.Value;
/**
 * 
 * @author Andrada Radu
 *
 */
@Value.Immutable
public interface Row {
	
	/**
	 * @return list with the spots from the row
	 */
	List<Spot> getRowList();
}
