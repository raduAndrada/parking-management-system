package parking;

import java.util.List;

/**
 * 
 * @author Andrada Radu
 * interface for the parking lot
 */
public interface ParkingLot {
	
	/**
	 * @return the list of floor for the parking lot
	 */
	List<Floor> getFloorList();

}
