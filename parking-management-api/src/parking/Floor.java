package parking;

import java.util.List;

/**
 * @author Andrada Radu
 *
 */
public interface Floor {

	/**
	 * @return list of rows from the floor
	 */
	List<Row> getRowList();
}
