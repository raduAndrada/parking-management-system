package parking;

import org.immutables.value.Value;

/**
 * 
 * @author Andrada Radu
 *
 */
@Value.Immutable
public interface Spot {
	
	/**
	 * @return size of a spot
	 */
	public SpotSize getSize();
}
