package vehicle;

public enum VehicleType {

	MOTORBIKE,
	CAR,
	BUS
}
