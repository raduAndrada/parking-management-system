package vehicle;

/**
 * @author Andrada Radu
 *
 */
public interface Vehicle {

	VehicleType getType();
}
