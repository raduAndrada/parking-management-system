package com.bia.business.impl.parking.exceptions;

public class InvalidParkingSpotArgumentException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidParkingSpotArgumentException(String errorMessage) {
	        super(errorMessage);
	    }

}
