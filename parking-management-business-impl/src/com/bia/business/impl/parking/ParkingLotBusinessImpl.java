package com.bia.business.impl.parking;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.bia.business.api.parking.ParkingLotBusiness;
import com.bia.business.impl.parking.exceptions.InvalidParkingSpotArgumentException;
import com.bia.model.base.SpotUpdateDetails;
import com.bia.model.parking.Floor;
import com.bia.model.parking.ImtFloor;
import com.bia.model.parking.ImtParkingLot;
import com.bia.model.parking.ImtRow;
import com.bia.model.parking.ImtSpot;
import com.bia.model.parking.ParkingLot;
import com.bia.model.parking.Row;
import com.bia.model.parking.Spot;
import com.bia.model.parking.SpotSize;
import com.bia.model.vehicle.Vehicle;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

/**
 *
 * @author Andrada Radu Implementation for the business logic
 */
public class ParkingLotBusinessImpl implements ParkingLotBusiness {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Spot> getAvailableSpots(ParkingLot parkingLot, Vehicle vehicle) {
		final List<Spot> availableSpots = findAvailableSpots(parkingLot);
		return findFirstAvaiableSpotForVehicle(availableSpots, vehicle);

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ParkingLot updateParkingLotSpots(final ParkingLot parkingLot, final SpotUpdateDetails updateDets) {
		return updateSpotsAvailability(parkingLot, updateDets);
	}

	/**
	 * Update the state of the parking lot
	 *
	 * @param parkingLot parking lot to update
	 * @param updateDets details for the update
	 */
	private ParkingLot updateSpotsAvailability(final ParkingLot parkingLot, final SpotUpdateDetails updateDets) {
		final Optional<Floor> floor = parkingLot.getFloorList().stream()
				.filter(f -> f.getNumber().equals(updateDets.getFloorNumber())).findFirst(); // get the floor of the
																								// occupied spot
		if (floor.isPresent()) {
			final Optional<Row> row = floor.get().getRowList().stream()
					.filter(r -> r.getNumber().equals(updateDets.getRowNumber())).findFirst(); // the the row of the
																								// occupied spot
			if (row.isPresent()) {
				final List<Spot> spotsOnRow = row.get().getSpotList().stream()
						.filter(s -> !s.getNumber().equals(updateDets.getSpotNumber())
								&& !(updateDets.isBusSpot() && s.getNumber().equals(updateDets.getSpotNumber() + 1))) // collect
																														// all
																														// the
																														// unchanged
																														// spots
						.collect(Collectors.toList());
				final List<Spot> spotsForUpdate = row.get().getSpotList().stream()
						.filter(s -> s.getNumber().equals(updateDets.getSpotNumber())
								|| (updateDets.isBusSpot() && s.getNumber().equals(updateDets.getSpotNumber() + 1))) // find
																														// the
																														// corresponding
																														// spot/s
						.collect(Collectors.toList());
				if (spotsForUpdate.isEmpty()) {
					throw new InvalidParkingSpotArgumentException("Number of spot is invalid");
				} else {
					spotsForUpdate.forEach(e -> spotsOnRow.add(ImtSpot.builder().from(e).isFree(!e.isFree()).build()));
				}
				// build the new object for update

				// updated row
				final Row updatedRow = ImtRow.builder().number(updateDets.getRowNumber()).spotList(spotsOnRow).build();
				final List<Row> updatedRows = floor.get().getRowList().stream()
						.filter(r -> !r.getNumber().equals(updateDets.getRowNumber())).collect(Collectors.toList());
				updatedRows.add(updatedRow);

				// update floor
				final Floor updatedFloor = ImtFloor.builder().number(updateDets.getFloorNumber()).rowList(updatedRows)
						.build();
				final List<Floor> updatedFloors = parkingLot.getFloorList().stream()
						.filter(f -> !f.getNumber().equals(updateDets.getFloorNumber())).collect(Collectors.toList());
				updatedFloors.add(updatedFloor);

				// build the parking lot
				return ImtParkingLot.builder().floorList(updatedFloors).build();
			} else {
				// error row not found
				throw new InvalidParkingSpotArgumentException("Number of row is invalid");
			}
		} else {
			// throw error floor not found
			throw new InvalidParkingSpotArgumentException("Number of floor is invalid");
		}
	}

	/**
	 * find all the available slots and sort them
	 *
	 * @param parkingLot parking lot to search in
	 * @return an ordered list with all the free slots
	 */
	private List<Spot> findAvailableSpots(final ParkingLot parkingLot) {
		final List<Floor> floors = parkingLot.getFloorList();
		final List<Spot> availableSpots = Lists.newArrayList();

		// build the list of all available spots
		for (final Floor floor : floors) {
			final List<Row> rows = floor.getRowList();
			for (final Row row : rows) {
				availableSpots.addAll(findFreeSpotOnRow(row));
			}
		}

		// sort the list in ascending order
		Collections.sort(availableSpots, new SortSpotByNumber());
		return availableSpots;

	}

	/**
	 * Get all the free spots on a row
	 *
	 * @param row the row to search for free slots
	 * @return all the free slots
	 */
	private List<Spot> findFreeSpotOnRow(final Row row) {
		return row.getSpotList().stream().filter(spot -> spot.isFree()).collect(Collectors.toList());
	}

	/**
	 * Method to determine the first available spot for a vehicle
	 *
	 * @param spots   all the free spots
	 * @param vehicle the vehicle for parking
	 * @return the closest available spot/s (for buses)
	 */
	private List<Spot> findFirstAvaiableSpotForVehicle(final List<Spot> spots, final Vehicle vehicle) {
		List<Spot> searchedSpots = Lists.newArrayList();

		// check if there are any spots
		if (spots.isEmpty()) {
			searchedSpots = ImmutableList.of();
		} else {
			switch (vehicle.getType()) {
			case MOTORBIKE: {
				// return the 1st spot for a motorbike
				searchedSpots = ImmutableList.of(spots.get(0));
				break;
			}
			case CAR: {
				// return the first big spot for a car
				final Optional<Spot> firstSpot = spots.stream().filter(spot -> spot.getSize().equals(SpotSize.BIG))
						.findFirst();
				if (firstSpot.isPresent()) {
					searchedSpots = ImmutableList.of(firstSpot.get());
				} else {
					searchedSpots = ImmutableList.of(); // no spots found
				}
				break;
			}
			case BUS: {
				// return 2 spots next to each other for a bus
				final List<Spot> bigSpots = spots.stream().filter(spot -> spot.getSize().equals(SpotSize.BIG))
						.collect(Collectors.toList());
				for (int i = 0; i < bigSpots.size() - 1; i++) {
					final Spot currentSpot = bigSpots.get(i);
					final Spot nextSpot = bigSpots.get(i + 1);
					if (currentSpot.getFloorNumber().equals(nextSpot.getFloorNumber())
							&& currentSpot.getRowNumber().equals(nextSpot.getRowNumber())
							&& currentSpot.getNumber().equals(nextSpot.getNumber() - 1)) {
						searchedSpots = ImmutableList.of(currentSpot, nextSpot);
						break;
					} else {
						searchedSpots = ImmutableList.of();
					}
				}
				break;
			}
			}

		}
		return searchedSpots;
	}

	/**
	 *
	 * @author Andrada Radu Class used to sort the spots
	 */
	class SortSpotByNumber implements Comparator<Spot> {
		@Override
		public int compare(Spot spot0, Spot spot1) {
			int temp;
			if (spot0.getFloorNumber() < spot1.getFloorNumber()) {
				temp = -1;
			} else if (spot0.getFloorNumber() > spot1.getFloorNumber()) {
				temp = 1;
			} else {
				if (spot0.getRowNumber() < spot1.getRowNumber()) {
					temp = -1;
				} else if (spot0.getRowNumber() > spot1.getRowNumber()) {
					temp = 1;
				} else {
					if (spot0.getNumber() < spot1.getFloorNumber()) {
						temp = -1;
					} else {
						temp = 1;
					}
				}
			}
			return temp;
		}

	}

}
