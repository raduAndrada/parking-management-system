package com.bia.business.impl.parking;

import java.util.List;
import java.util.Random;

import com.bia.business.api.parking.ParkingLotGenerator;
import com.bia.model.parking.Floor;
import com.bia.model.parking.ImtFloor;
import com.bia.model.parking.ImtParkingLot;
import com.bia.model.parking.ImtRow;
import com.bia.model.parking.ImtSpot;
import com.bia.model.parking.ParkingLot;
import com.bia.model.parking.Row;
import com.bia.model.parking.Spot;
import com.bia.model.parking.SpotSize;
import com.google.common.collect.Lists;

/**
 *
 * @author Andrada Radu This class is used to generate an initial parking lot
 *         with available spots
 */
public class ParkingLotGeneratorImpl implements ParkingLotGenerator {

	private static final int MAX_NUMBER_OF_FLOORS = 6; // maximum number of floors for the parking lot
	private static final int MAX_NUMBER_OF_ROWS = 30; // maximum number of rows for the parking lot
	private static final int MAX_NUMBER_OF_SPOTS = 10; // maximum number of spots for the parking lot

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ParkingLot generateParkingLot() {

		final Random rand = new Random();
		final Integer numberOfFloors = rand.nextInt(MAX_NUMBER_OF_FLOORS) + 1; // 0 is the ground floor
		final Integer numberOfRows = rand.nextInt(MAX_NUMBER_OF_ROWS) + 1; // floors must have at least 1 row
		final Integer numberOfSpots = rand.nextInt(MAX_NUMBER_OF_SPOTS) + 1; // rows must have at least 1 spot

		final List<Floor> floorList = Lists.newArrayList();
		for (int i = 1; i <= numberOfFloors; i++) {
			final List<Row> rowList = Lists.newArrayList();
			for (int j = 1; j <= numberOfRows; j++) {
				final List<Spot> spots = Lists.newArrayList();
				for (int k = 1; k <= numberOfSpots; k++) {
					spots.add(ImtSpot.builder().floorNumber(i).rowNumber(j).number(k).isFree(rand.nextBoolean())
							.size(rand.nextBoolean() ? SpotSize.BIG : SpotSize.SMALL).build());
				}
				rowList.add(ImtRow.builder().number(j).spotList(spots).build());
			}
			floorList.add(ImtFloor.builder().number(i).rowList(rowList).build());
		}
		return ImtParkingLot.builder().floorList(floorList).build();
	}
}
