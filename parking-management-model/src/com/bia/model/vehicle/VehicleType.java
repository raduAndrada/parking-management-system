package com.bia.model.vehicle;

public enum VehicleType {

	MOTORBIKE,
	CAR,
	BUS
}
