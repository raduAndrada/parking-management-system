package com.bia.model.vehicle;

import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * @author Andrada Radu
 *
 */
@Value.Immutable
@JsonSerialize(as = ImtVehicle.class)
@JsonDeserialize(builder = ImtVehicle.Builder.class)
public interface Vehicle {

	/**
	 * @return MOTOBIKE, CAR or BUS
	 */
	VehicleType getType();

}
