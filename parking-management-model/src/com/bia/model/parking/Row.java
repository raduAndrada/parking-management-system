package com.bia.model.parking;


import java.util.List;

import org.immutables.value.Value;
/**
 * 
 * @author Andrada Radu
 *
 */
@Value.Immutable
public interface Row {
	
	/**
	 * @return the number of the row
	 */
	Integer getNumber();
	
	/**
	 * @return list with the spots from the row
	 */
	List<Spot> getSpotList();
}
