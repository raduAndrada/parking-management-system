package com.bia.model.parking;


import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;


/**
 * 
 * @author Andrada Radu
 *
 */
@Value.Immutable
@Value.Modifiable
@JsonSerialize(as = ImtSpot.class)
@JsonDeserialize(builder = ImtSpot.Builder.class)
public interface Spot {

	/**
	 * @return number of the floor for the spot
	 */
	 Integer getFloorNumber();

	/**
	 * @return number of the row for the spot
	 */
	 Integer getRowNumber();

	/**
	 * @return the number of the spot
	 */
	 Integer getNumber();

	/**
	 * @return size of a spot
	 */
	 SpotSize getSize();

	/**
	 * @return true if the spot is not occupied false otherwise
	 */
	boolean isFree();
	

}
