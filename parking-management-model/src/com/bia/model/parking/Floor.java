package com.bia.model.parking;

import java.util.List;

import org.immutables.value.Value;

/**
 * @author Andrada Radu
 *
 */
@Value.Immutable
public interface Floor {
	
	/**
	 * @return the number of the row
	 */
	public Integer getNumber();

	/**
	 * @return list of rows from the floor
	 */
	public List<Row> getRowList();
	

}
