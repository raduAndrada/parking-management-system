package com.bia.model.parking;

import java.util.List;

import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * 
 * @author Andrada Radu interface for the parking lot
 */
@Value.Immutable
@JsonSerialize(as = ImtParkingLot.class)
@JsonDeserialize(builder = ImtParkingLot.Builder.class)
public interface ParkingLot {

	/**
	 * @return the list of floor for the parking lot
	 */
	List<Floor> getFloorList();

	/*
	 * I wanted to use different objects and classes for this. An alternative would
	 * be to simply keep a map: TreeMap<Spot,boolean> The system could access
	 * determine which spot is free by finding the first entry with the value false.
	 * The map could also be SortedMap<Spot,Vehicle> if the vehicle has a plate
	 * number. Finding the first free spot would then become finding the first entry
	 * with a null value in the map.
	 * 
	 */

}
