package com.bia.model.base;

import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * 
 * @author Andrada Radu
 *
 */
@Value.Immutable
@JsonSerialize(as = ImtSpotUpdateDetails.class)
@JsonDeserialize(builder = ImtSpotUpdateDetails.Builder.class)
public interface SpotUpdateDetails {

	/**
	 * @return number of the spot
	 */
	public Integer getSpotNumber();

	/**
	 * @return number of the row
	 */
	public Integer getRowNumber();

	/**
	 * @return number of the floor
	 */
	public Integer getFloorNumber();

	/**
	 * @return true if update is for bus false otherwise
	 */
	public boolean isBusSpot();
}
